# OpenML dataset: OpenML_trainingdata_placings

https://www.openml.org/d/41175

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Hi OpenML Community,

I&rsquo;m new to ML, and after looking at the data available; I decided that the best way to learn is to go through the process from the ground up. I took a well beaten problem, is it possible to predict the outcome of a horse race using ML?

Here&rsquo;s a dataset that took me 3 weeks to pull together. It is a sample of 17 meetings.

Odds  : current race closing odds
weight  : in kg
barrier  : drawn barrier
jockey       : win strike rate
trainer      : win strike rate
L1_pos    : previous race finish position
L1_class  : previous race class
L1_dist  : previous race distance
L1_wt     : previous race weight
L2_pos    : previous 2 race finish position
L2_class  : previous 2 race class
L2_dist  : previous 2 race distance
L2_wt             : previous 2 race weight
L3_pos    : previous 3 race finish position
L3_class  : previous 3 race class
L3_dist  : previous 3 race distance
L3_wt             : previous 3 race weight
R_class  : current race class
Dist          : current race distance
Placing      : Result of current race 1 - Win, 0 - Place (2/3), -1 unplaced
Using weka.classifiers.functions.SMO, and all default settings I&rsquo;m getting 97.77% correctly classified.
Correctly Classified Instances        1188               97.7778 %
Incorrectly Classified Instances        27                2.2222 %
Kappa statistic                          0.9481
Mean absolute error                      0.2272
Root mean squared error                  0.281 
Relative absolute error                 78.2269 %
Root relative squared error             73.7888 %
Total Number of Instances             1215

If I split it 66% train/34% Test, it drops to 57.8%. If I ran the full training set with 10 folds cross-validation I get 57.86%. 
I&rsquo;m trying to understand how to interpret the results, for example is the 97.77% invalid? and it is closer to W.8. Any thoughts?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41175) of an [OpenML dataset](https://www.openml.org/d/41175). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41175/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41175/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41175/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

